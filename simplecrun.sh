#!/bin/bash 


if [ $# -ne 2 ]; then
   echo "Usage: $0 <procs> <target directory>" 
   exit 1;
fi

PROCS=${1:-1}
TESTDIR=${2:-/root/testdir/deleteme/}
LOGFILE="`hostname -s`-`date +%Y%m%d%H%M`.out";

#echo  "Cleaning up $TESTDIR:" | tee -a $LOGFILE
#echo -n "Files: "
#find $TESTDIR -type f | wc -l | tee -a $LOGFILE
#DELSTART=`date +'%s'`
#rm -rf $TESTDIR 2> /dev/null
#DELEND=`date +'%s'`
#echo "Delete Time: $(($DELEND - $DELSTART)) sec " | tee -a $LOGFILE

echo "Creating $TESTDIR" | tee -a $LOGFILE
mkdir -p $TESTDIR 2> /dev/null



date > $LOGFILE
uname -a | tee -a $LOGFILE
# Are we an NFS directory?

TESTDIR=`realpath $TESTDIR`

NFSSERVER=`df -h $TESTDIR | tail -1 | grep ':' | awk -F: '{print $1}'`
if [ ! -z "$NFSSERVER" ]; then 
	echo "$TESTDIR is an NFS share" | tee -a $LOGFILE
	SHARE=`df $TESTDIR | tail -n 1 | awk '{print $(NF)}'`
	SERVER=`df $TESTDIR | tail -n 1 | awk -F ':' '{print $1}'`
	grep $SHARE /proc/mounts
	ping -c 10 $SERVER | tee -a $LOGFILE
else
	echo "$TESTDIR is local" | tee -a $LOGFILE
fi

echo "===" | tee -a $LOGFILE


for i in `seq 1 $PROCS`; do 
   ./writer $i $TESTDIR | tee -a $LOGFILE; 
#   echo -n "Files: " | tee -a $LOGFILE
#   find $TESTDIR -type f | wc -l | tee -a $LOGFILE
#   echo -n "Size: " | tee -a $LOGFILE
#   du -sh $TESTDIR | tee -a $LOGFILE
done
