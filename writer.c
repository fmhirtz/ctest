#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>
#include <sys/time.h>
#include <limits.h>

#include <errno.h>


typedef struct varStruct {
   int thread_num;
   char target_dir[100];
   int files;
}writer_vars; 


void * writer(void *argp)
{
   writer_vars *myVars;
   myVars = (writer_vars *) argp;

   FILE *targetFile;

   int counter, fd = 0;
   char filename[100] = {'\0'};
   char threaddir[100] = {'\0'};
   char chunk[3*1024] = { '\0' };
   memset(chunk,'4',sizeof(chunk));	

   //We'll have each thread write to a seperate subdirectory
   sprintf(threaddir, "%s/%d", myVars->target_dir, myVars->thread_num);
   mkdir(threaddir, S_IRWXU);

   do 
   {
      sprintf(filename, "%s/testfile.%d.%d", threaddir, myVars->thread_num, counter);
      
      targetFile = fopen(filename,"w");
      if(targetFile == NULL) {
         perror("fopen failed: ");
         exit(EXIT_FAILURE);
      }
      if(fputs(chunk, targetFile) == EOF) {
         perror("fputs failed: ");
         exit(EXIT_FAILURE);
      }
      fclose(targetFile);
      counter++;
   } while( counter < myVars->files);

   free(myVars);
}

void usage( char* program) {
   printf("Usage: %s <threads> <directory>\n", program);
   fflush(NULL);
   exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
   
   struct timeval  tv1, tv2;
   gettimeofday(&tv1, NULL);

   char *endptr, *str;
   long threads;
   char directory[100];

   if(argc < 3) {
      usage(argv[0]);
      exit(EXIT_FAILURE);
   }
   strcpy(directory, argv[2]);
   int rc, t = 0;   

   errno = 0;
   str = argv[1];
   threads = strtol(str, &endptr, 10);
   if ((errno == ERANGE && (threads == LONG_MAX ||threads == LONG_MIN))
            || (errno != 0 && threads == 0)) {
        perror("strtol");
        exit(EXIT_FAILURE);
    }

   if (endptr == str) {
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }
   const int files = 10000;


   printf("Starting %d threads, write directory %s\n", threads, directory);
   fflush(NULL);

   pthread_t writerThread[threads];

   writer_vars **threadVars;
   threadVars = (writer_vars **)malloc(threads*sizeof(writer_vars));

   mkdir(argv[2], S_IRWXU);


   for( t=0; t < threads; t++)
   {
      threadVars[t] = (writer_vars *)calloc(1,sizeof(writer_vars));
      threadVars[t]->thread_num = t;
      strcpy(threadVars[t]->target_dir, directory);
      threadVars[t]->files = files;
   }
   for( t=0; t < threads; t++)
   {
      //printf("Creating thread: %3d  Files: %6d Directory: %-30s\n", threadVars[t]->thread_num, threadVars[t]->files, threadVars[t]->target_dir);
      rc = pthread_create(&writerThread[t], NULL, writer, threadVars[t]);
      if (rc) {
         printf("ERROR; return code from pthread_create() is %d\n", rc);
         fflush(NULL);
         exit(-1);
      }
   }  
   for( t=0; t < threads; t++)
   {
      pthread_join(writerThread[t], NULL);
   }
   
   gettimeofday(&tv2, NULL);

   printf ("Total time = %f seconds\n",
         (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
         (double) (tv2.tv_sec - tv1.tv_sec));
   fflush(NULL);
   pthread_exit(NULL);
   

   return 0;
}

